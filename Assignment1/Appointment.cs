﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1
{
    public class Appointment
    {
        //Appointment info:
        //Start time of appointment
        //Appointment duration
        //Type of additional service requested

        public string apptStart;
        public int apptDuration;
        public List<string> service;

        public Appointment(string start, int duration, List<string> s)
        {
            apptStart = start;
            apptDuration = duration;
            service = s;
        }
    }
}