﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1.aspx.cs" Inherits="Assignment1.Assignment1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div>
                <h2>Dog Day Care Service</h2>
                <label for="clientName">Name</label>
                <asp:TextBox runat="server" ID="ownerName" name="clientName"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your name" ControlToValidate="ownerName" ID="validatorName"></asp:RequiredFieldValidator>
            </div>
            <div id="test" runat="server">
                <label>Phone Number</label>
                <asp:TextBox ID="phoneNumber" runat="server"></asp:TextBox>
                <asp:RegularExpressionValidator ID="validatorPhoneFormat" runat="server" ErrorMessage="Invalid Phone Number Format" ControlToValidate="phoneNumber" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" ></asp:RegularExpressionValidator> 
                <%--Referenced from https://stackoverflow.com/questions/9732455/how-to-allow-only-integers-in-a-textbox--%>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your phone number" ControlToValidate="phoneNumber" ID="validatorPhoneNumber"></asp:RequiredFieldValidator>


            </div>
            <div>
                <label for="clientEmail">Email</label>
                <asp:TextBox ID="ownerEmail" runat="server" placeholder="e.g. johndoe@domain.com" Font-Names="clientEmail"></asp:TextBox>
                <asp:RegularExpressionValidator ID="emailFormatValidation" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="ownerEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
                <%--Referenced from https://stackoverflow.com/questions/182542/email-address-validation-for-asp-net--%>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your email." ControlToValidate="ownerEmail" ID="validatorEmail"></asp:RequiredFieldValidator>

            </div>
            
            <div>
                <label for="dogName">Name of your Dog</label>
                <asp:TextBox runat="server" ID="dogName" name="dogName"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter the name of your Dog" ControlToValidate="dogName" ID="validatorDogName"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label for="dogBreed">Breed</label>
                <asp:TextBox runat="server" ID="dogBreed" name="dogBreed"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter the breed of your dog" ControlToValidate="dogBreed" ID="validatorDogBreed"></asp:RequiredFieldValidator>
            </div>

            <div>
                <label for="dogSex">Sex:</label>
                <asp:RadioButtonList ID="dogSex" runat="server" name="dogSex">
                <asp:ListItem ID="DogSexMale" Value="male">Male</asp:ListItem>
                <asp:ListItem ID="DogSexFemale" Value="female">Female</asp:ListItem>
                </asp:RadioButtonList>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please select the sex of your dog" ControlToValidate="dogSex" ID="validatorDogSex"></asp:RequiredFieldValidator>
            </div>
            <div>
                <label for="apptStart">Drop-off Time</label>
                <asp:DropDownList runat="server" ID="appointmentStartTime" name="apptStart">
                    <asp:ListItem Text="08:00AM" Value="08:00"></asp:ListItem>
                    <asp:ListItem Text="08:30AM" Value="08:30"></asp:ListItem>
                    <asp:ListItem Text="09:00AM" Value="09:00"></asp:ListItem>
                    <asp:ListItem Text="09:30AM" Value="09:30"></asp:ListItem>
                    <asp:ListItem Text="10:00AM" Value="10:00"></asp:ListItem>
                    <asp:ListItem Text="10:30AM" Value="10:30"></asp:ListItem>
                    <asp:ListItem Text="11:00AM" Value="11:00"></asp:ListItem>
                    <asp:ListItem Text="11:30AM" Value="11:30"></asp:ListItem>
                    <asp:ListItem Text="12:00PM" Value="12:00"></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div>
                <label for="apptDuration">How many hours will your dog be at this service for? (Please note you cannot use this service for anything less than 3 hours)</label>
                <br />
                <asp:TextBox runat="server" ID="durationOfAppointment" name="apptDuration"></asp:TextBox>
                <asp:RangeValidator runat="server" ControlToValidate="durationOfAppointment" Type="Integer" MinimumValue="3" MaximumValue="8" ErrorMessage="Please enter a value between 3 and 8"></asp:RangeValidator>
                <asp:RequiredFieldValidator runat="server" ErrorMessage="Please indicate the number of hours you will be using this service" ControlToValidate="durationOfAppointment" ID="validatorHoursOfService"></asp:RequiredFieldValidator>

            </div>

            <div id="desiredService" runat="server">
                <label for="service">Additional Service Request</label>
                <div>
                     <asp:checkBox ID="grooming" Text="Grooming" runat="server" name="service"/>
                     <asp:CheckBox ID="shampoo" Text="Shampoo" runat="server" name="service"/>
                     <asp:CheckBox ID="massage" Text="Massage" runat="server" name="service"/>
                </div>
               
            </div>

            <div>
                <label for="userAgreement">I have read and agree to the terms and conditions for this service indicated <a href="https://imgur.com/gallery/AfgkjCf" target="_blank">here</a>.</label>
                <div>
                    <asp:checkBox id="termsAndConditions" text="Accept Terms" Runat="server" name="userAgreement" />
                    <div>
                        <asp:CustomValidator id="termsValidator" ErrorMessage="Please agree to the terms and conditions before proceeding." runat="server" onservervalidate="termsValidation"></asp:CustomValidator>
                    </div>
                </div>
            </div>

            <div id="promoAndNews" runat="server">
                <label for="promoAndNews">Would you like to receive promotions and other related news about this service?</label>
                <div>
                     <asp:checkBox id="receivePromo" text="Yes, I would like to receive news about this service" Runat="server" name="promoAndNews" />
                </div>     
            </div>

            <div>
                <asp:Button runat="server" ID="myButton" Text="Confirm Appointment" OnClick="clientClick" />
            </div>

             <asp:ValidationSummary runat="server" id="validationSummary" DisplayMode="BulletList" HeaderText="Please ensure that you have entered the following fields:" />
            <%--Referenced from https://docs.microsoft.com/en-us/dotnet/api/system.web.ui.webcontrols.validationsummary?view=netframework-4.7.2--%>
        </div>
    </form>

      <div runat="server" id="bookingConfirmation">

      </div>
</body>
</html>

<%--Things changed from Assignment 1:
        -Added "for" attributes on labels, and "name" attributes on the corresponding elements
        -Added an Additional Service Request checkbox field, which returns a list of desired service by the user.
        -Added a div at the bottom of the page, where the Booking class method injects the booking confirmation details.
        -Custom validator added to check whether the user agrees to terms and conditions. 

    --%>


