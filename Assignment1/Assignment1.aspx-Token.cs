﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class Assignment1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        protected void clientClick(object sender, EventArgs e)
        {
            string dogOwnerName = ownerName.Text;
            string clientPhoneNumber = ownerPhoneNumber.Text;
            string clientEmail = ownerEmail.Text;
            string nameOfDog = dogName.Text;
            string breedOfDog = dogBreed.Text;
            string sexOfDog = dogSex.Text;
            string apptStart = appointmentStartTime.Text;
            int apptDuration = int.Parse(durationOfAppointment.Text);

            Client newClient = new Client();
            newClient.DogOwnerName = dogOwnerName;
            newClient.ClientPhoneNum = clientPhoneNumber;
            newClient.ClientEmail = clientEmail;

            Dog newDog = new Dog(nameOfDog, sexOfDog, breedOfDog);
            newDog.nameOfDog = nameOfDog;
            newDog.dogBreed = breedOfDog;
            newDog.sexOfDog = sexOfDog;

            Appointment newAppointment = new Appointment(apptStart, apptDuration);
            newAppointment.apptStart = apptStart;
            newAppointment.apptDuration = apptDuration;



            Booking newBooking = new Booking(newClient, newDog, newAppointment);


           testDiv.InnerHtml = newBooking.bookingConfirmation();
        }
    }
}