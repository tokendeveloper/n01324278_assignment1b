﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Assignment1
{
    public partial class Assignment1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void termsValidation(object o, ServerValidateEventArgs e)
        {
            if (termsAndConditions.Checked)
            {
                e.IsValid = true;
            }
            else
            {
                e.IsValid = false;
            }
        }
        //Referenced from https://stackoverflow.com/questions/1228112/how-do-i-make-a-checkbox-required-on-an-asp-net-form //
        //If terms and conditions validation checkbox is checked, validation returns true. Else, validation fails.

        protected void clientClick(object sender, EventArgs e)
        {
            //referenced from Christine's pizza order codebehind.
            if (!Page.IsValid)
            {
                return;
            }


            //Variable declaration----------------//

            string dogOwnerName = ownerName.Text;
            string clientPhoneNumber = phoneNumber.Text;
            string clientEmail = ownerEmail.Text;
            string nameOfDog = dogName.Text;
            string breedOfDog = dogBreed.Text;
            string sexOfDog = dogSex.Text;
            string apptStart = appointmentStartTime.Text;
            int apptDuration = int.Parse(durationOfAppointment.Text);
            List<string> serviceNeeded = new List<string> { };
            bool getPromo = receivePromo.Checked;

            //Client class object//
            Client newClient = new Client(getPromo);
            newClient.DogOwnerName = dogOwnerName;
            newClient.ClientPhoneNum = clientPhoneNumber;
            newClient.ClientEmail = clientEmail;
            newClient.promoRequested = getPromo;
            
            //Dog class object//
            Dog newDog = new Dog(nameOfDog, sexOfDog, breedOfDog);
            newDog.nameOfDog = nameOfDog;
            newDog.dogBreed = breedOfDog;
            newDog.sexOfDog = sexOfDog;

            //Appointmnet class object//
            Appointment newAppointment = new Appointment(apptStart, apptDuration, serviceNeeded);
            newAppointment.apptStart = apptStart;
            newAppointment.apptDuration = apptDuration;

            //List<string> newServiceRequest = new List<string>();

            foreach (Control control in desiredService.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox service = (CheckBox)control;
                    if (service.Checked)
                    {
                        serviceNeeded.Add(service.Text);
                    }
                }
            }
            newAppointment.service = serviceNeeded.ToList();
            //Referenced from Christine's pizza order//
            //Pushes checkbox items into the list variable "serviceNeeded"//

            //Booking class object//
            Booking newBooking = new Booking(newClient, newDog, newAppointment);           
        
            foreach (Control control in promoAndNews.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox promo = (CheckBox)control;
                    if (promo.Checked)
                    {
                        getPromo = true;
                    }
                }
            }
            //Referenced from Christine's pizza order//
            //If the user checks the promo and newsletter checkbox, promoRequested property of Client object will return true.

            bookingConfirmation.InnerHtml = newBooking.bookingConfirmation();
        }


    }
}