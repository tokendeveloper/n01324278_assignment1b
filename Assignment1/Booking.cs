﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1
{
    public class Booking
    {
        //Booking consists of the following information
        //Client info
        //Client's Dog's info
        //Appointment info

        public Client client;
        public Dog dog;
        public Appointment appointment;

        public Booking(Client c, Dog d, Appointment a)
        {
            client = c;
            dog = d;
            appointment = a;
        }

        

        public string bookingConfirmation()
        {
            string confirmation = "<br> Booking Confirmation: <br>";
            confirmation += "Client Name: " + client.DogOwnerName + "<br>";
            confirmation += "Phone Number: " + client.ClientPhoneNum + "<br>";
            confirmation += "E-mail: " + client.ClientEmail + "<br>";
            confirmation += "Client's Dog: " + dog.nameOfDog + "<br>";
            confirmation += "Breed: " + dog.dogBreed + "<br>";
            confirmation += "Sex: " + dog.sexOfDog + "<br>";
            confirmation += "Appointment Start Time: " + appointment.apptStart + "<br>";
            confirmation += "Appointment Duration: " + appointment.apptDuration + " hours " + "<br>";
            confirmation += "Service Requested: " + String.Join(", ", appointment.service.ToArray()) + "<br>";
            confirmation += "Total Cost: $" + calcCost(appointment.apptDuration, appointment.service) + "<br>";
            if (client.promoRequested == true)
            {
                confirmation += "You are signed up to receive promotional materials and newsletters." + "<br>";
            }
           
            return confirmation;
        }

        public double calcCost(int duration, List<string> services)
        {
            appointment.apptDuration = duration;
            appointment.service = services;

            double cost = 8 * duration;

            for (int i = 0; i < appointment.service.ToArray().Length; i++)
            {
                if (appointment.service.ToArray()[i] == "Grooming")
                {
                    cost += 10;
                } 
                else if (appointment.service.ToArray()[i] == "Shampoo")
                {
                    cost += 15;
                }
                else if (appointment.service.ToArray()[i] == "Massage")
                {
                    cost += 12;
                }
            }
            return cost;
        }


    }
}