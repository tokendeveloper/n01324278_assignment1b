﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment1
{
    public class Client
    {
        //Info for clients 
        //Name
        //Phone#
        //Email

        private string dogOwnerName;
        private string clientPhoneNum;
        private string clientEmail;
        public bool promoRequested;

        public Client(bool p)
        {
            promoRequested = p;
        }

        public string DogOwnerName
        {
            get { return dogOwnerName; }
            set { dogOwnerName = value; }
        }

        public string ClientPhoneNum
        {
            get { return clientPhoneNum; }
            set { clientPhoneNum = value; }
        }

        public string ClientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }

    }
}